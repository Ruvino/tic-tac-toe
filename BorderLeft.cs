using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderLeft : Borders
{
    public override void OnCollisionEnter2D(Collision2D collision)
    {
        collision.rigidbody.AddForce(transform.right * m_Thrust, ForceMode2D.Impulse);
    }
}
