using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderUp : Borders
{
    public override void OnCollisionEnter2D(Collision2D collision)
    {
        collision.rigidbody.AddForce(-transform.up * m_Thrust, ForceMode2D.Impulse);
    }
}
