using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Borders : MonoBehaviour
{
    [SerializeField] protected float m_Thrust = 20f;

    public abstract void OnCollisionEnter2D(Collision2D collision);   
}
