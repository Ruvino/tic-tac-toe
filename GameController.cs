using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text[] spaceList;
    public GameObject[] winlineList;
    public GameObject gameOverPanel;
    public Text gameOverText;
    public Button checkBtn, randomBtn;

    private string side;
    private int winCounter = 0;

    private void CheckMatch()
    {
        if (spaceList[0].text == spaceList[1].text && spaceList[1].text == spaceList[2].text)
        {
            side = spaceList[0].text;
            DrawWinLine(0);
        }
        else if (spaceList[3].text == spaceList[4].text && spaceList[4].text == spaceList[5].text)
        {
            side = spaceList[3].text;
            DrawWinLine(1);
        }
        else if (spaceList[6].text == spaceList[7].text && spaceList[7].text == spaceList[8].text)
        {
            side = spaceList[6].text;
            DrawWinLine(2);
        }
        else if (spaceList[0].text == spaceList[3].text && spaceList[3].text == spaceList[6].text)
        {
            side = spaceList[0].text;
            DrawWinLine(3);
        }
        else if (spaceList[1].text == spaceList[4].text && spaceList[4].text == spaceList[7].text)
        {
            side = spaceList[1].text;
            DrawWinLine(4);
        }
        else if (spaceList[2].text == spaceList[5].text && spaceList[5].text == spaceList[8].text)
        {
            side = spaceList[2].text;
            DrawWinLine(5);
        }
        else if (spaceList[0].text == spaceList[4].text && spaceList[4].text == spaceList[8].text)
        {
            side = spaceList[0].text;
            DrawWinLine(6);
        }
        else if (spaceList[2].text == spaceList[4].text && spaceList[4].text == spaceList[6].text)
        {
            side = spaceList[2].text;
            DrawWinLine(7);
        }

        GameOver(side);
    }


    private void PutElementsRandom()
    {
        for (int i = 0; i < spaceList.Length; i++)
        {
            spaceList[i].text = PickRandomFromList();
        }

        CheckCorrect();

        checkBtn.interactable = true;
        randomBtn.interactable = false;
    }


    private void GameOver(string side)
    {
        gameOverPanel.SetActive(true);

        if (side == null)
            gameOverText.text = "Tie!";
        else
            gameOverText.text = side + " wins!";
  
        checkBtn.interactable = false;
        randomBtn.interactable = false;
    }


    private string PickRandomFromList()
    {
        string[] XO = new string[] { "X", "O" };
        string random�haracter = XO[Random.Range(0, XO.Length)];
        return random�haracter;
    }

    private void CheckCorrect()
    {
        winCounter = 0;

        if (spaceList[0].text == spaceList[1].text && spaceList[1].text == spaceList[2].text)
            winCounter++;
        if (spaceList[3].text == spaceList[4].text && spaceList[4].text == spaceList[5].text)
            winCounter++;
        if (spaceList[6].text == spaceList[7].text && spaceList[7].text == spaceList[8].text)
            winCounter++;
        if (spaceList[0].text == spaceList[3].text && spaceList[3].text == spaceList[6].text)
            winCounter++;
        if (spaceList[1].text == spaceList[4].text && spaceList[4].text == spaceList[7].text)
            winCounter++;
        if (spaceList[2].text == spaceList[5].text && spaceList[5].text == spaceList[8].text)
            winCounter++;
        if (spaceList[0].text == spaceList[4].text && spaceList[4].text == spaceList[8].text)
            winCounter++;
        if (spaceList[2].text == spaceList[4].text && spaceList[4].text == spaceList[6].text)
            winCounter++;

        if (winCounter > 1)
            PutElementsRandom();

    }

    void DrawWinLine(int i)
    {
        winlineList[i].SetActive(true);
    }

}
